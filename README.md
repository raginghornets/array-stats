# array-stats

## Description
A command-line-based python script which calculates basic statistics and plots a bar chart of a given array of numbers.

## Usage
In your computer's terminal, assuming your working directory is in your cloned repo, type in `python array-stats.py`. The app will ask you for your array length and the numbers in your array.
